const express = require('express');
const expressLayouts = require('express-ejs-layouts');

const app = express();
const PORT = process.env.PORT || 5000;

//MIDDLEWARES

//ejs
app.use(expressLayouts);
app.set('view engine', 'ejs');
app.use(express.static(__dirname + '/public'));

//Routes
app.use('/', require('./routes/web'));
app.use('/auth', require('./routes/auth'));


app.listen(PORT, console.log(`Server started on ${PORT}`));